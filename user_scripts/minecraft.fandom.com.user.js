// ==UserScript==
// @name        minecraft.fandom.com.user.js
// @namespace   https://gitlab.com/clking/
// @description automatically select language variant zh-tw
// @include     https://minecraft.fandom.com/zh/*
// @version     1
// @grant       none
// ==/UserScript==

(function() {
    var old_title = document.title;

    var path = location.pathname;

    var anchors = document.querySelectorAll('a');
    var altered = 0;
    for (var i = 0 ; i < anchors.length ; ++i) {
        document.title = [1 + i, anchors.length].join('/') + ' scanned';
        var a = anchors[i];
        var url = a.href;
        var match = /(?:(https?:)?)\/\/(minecraft\.fandom\.com)\/zh\/wiki\/([^\?]*)/.exec(url);

        if (!match) {
            continue;
        }

        var protocol, host, item;
        protocol = match[1];
        host = match[2];
        item = match[3];

        if ('Minecraft_Wiki' == path || /^[A-Z][A-Za-z_]*:/.test(path)) {
           continue;
        }

        var new_url = protocol + '//' + host + '/zh/wiki/' + item + '?variant=zh-tw';
        a.href = new_url;
        ++altered;
    }

    document.title = old_title;
})();
